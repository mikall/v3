from PIL import Image, ImageDraw
import math
import csv

f = open('MLSShotsData2011-2017.csv', encoding = "ISO-8859-1")
csv_f = csv.reader(f)

data = list(csv_f)

source_img = Image.open("field.jpg")
field = ImageDraw.Draw(source_img, "RGBA")

class dataPoint:
    def __init__(self, angle, hypotenuse, result):
        angle = math.radians(angle)

        adjacent = abs(math.cos(angle) * (hypotenuse / 115))
        opposite = (math.sqrt(abs((hypotenuse * hypotenuse / 115 / 115) - (adjacent * adjacent))))

        widthRatio = source_img.size[0]
        middle = ( source_img.size[1] / 2 )

        if angle < 0:
            opposite = opposite * -1

        adjacent = adjacent * widthRatio
        opposite = opposite * source_img.size[1] + middle

        self.angle = angle
        self.hypo = hypotenuse
        self.adjacent = adjacent
        self.opposite = opposite
        self.result = result

    def draw_rectangle(self):
        for layer in range(5):
            halfWidth = 4.5
            rect_start = (self.adjacent - halfWidth + layer, self.opposite - halfWidth + layer)
            rect_end = (self.adjacent + halfWidth - layer, self.opposite + halfWidth - layer)
            if (self.result).lower() == "goal":
                #orang-ish / red-ish
                field.rectangle((rect_start, rect_end), outline = (218, 52, 52, 15))
            else:
                #blue
                field.rectangle((rect_start, rect_end), outline = (63, 51, 217, 10))

valueErrorcount = 0

for row in data:
    if row != data[0]:
        dp = dataPoint(float(row[14]), float(row[13]), row[20])
        dp.draw_rectangle()

source_img.show()
source_img.save("output.png", "PNG")
