import csv
import numpy as np

f = open('MLSShotsData2011-2017.csv', encoding = "ISO-8859-1")
csv_f = csv.reader(f)
data = list(csv_f)

#Set start weights
weights = np.array([-0.1, 0.15, 0.03, 0.04])
alpha = 0.00001

minimum = [[0], weights]

#Used to create
def vectMult(data):
    global weights

    nums = list()
    if len(data) != len(weights):
        raise Exception("\n\nLength of weights and inputs don't match")
    try:
        for x in range(0, len(data)):
            nums.append(data[x] * weights[x])
    except TypeError:
        nums.append(data[x])

    return nums

def train(data):
    global weights

    #Goal, Miss, Blocked
    true = 0
    if str(data[20]) == "Goal":
        true = 1

    if data[18] == "NA":
        data[18] = 0

    total = 0
    #Take needed data from row
    condensedData = np.array([float(data[13]), abs(float(data[14])), float(data[18]), float(data[15])])
    error = 1

    #Do until the weights multiplied by data creates an acurate prediction
    while error > 0.1:
        #Mult data by weights to create outputs
        output = vectMult(condensedData)

        #Add all the outputs
        for x in output:
            total += x

        #Find how much the outputs missed by
        error = total - true

        #Alter weights
        for x in range(0, len(weights)):
            weights[x] = weights[x] - (alpha * (error * condensedData[x]))

def check():
    global weights
    global data

    for x in data[0:50000]:
        if x != data[0]:

            true = 0
            if str(data[20]) == "Goal":
                true = 1

            total = 0
            condensedData = np.array([float(data[13]), abs(float(data[14])), float(data[18]), float(data[15])])

            output = vectMult(condensedData)

            for x in output:
                total += x

            error = total - true

            if error > 0.5:
                if error < minimum[0]:
                    minimum[1] = weights

#  ----------------------------------------------  #
#                                                  #
#  Main body function to call the other functions  #
#                                                  #
#  ----------------------------------------------  #

def main(data):
    for x in data[0:50000]:
        if not (x == data[0]):
            train(x)

            print(weights)

#Call the main func and then print the final weights

main(data)

numbers = []
for num in weights:
    numbers.append(num)

print(numbers)
