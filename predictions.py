from PIL import Image, ImageDraw
import math
import csv
import numpy as np
import os
from datetime import datetime

#
#    Set Constants for later use in the main body function and in the functions
#

#set weights
#          Dist                    Angle                   Height                Available
weights = [-0.0022107819935719905, 0.00033603712557813656, 9.69683117601972e-05, 0.012574798306036278]

#set constants
PATH = "/home/mitchell/Desktop/V3/outputs"
FIELDWIDTH = 115
FIELDHEIGHT = 75
distanceRanges = [[0, 10], [11, 20], [21, 30], [31, 40], [41, 50], [51, 60], [61, 70], [71, 80], [81, 90], [91, 100], [101, 110]]
hgtAverage = [4.5940349049231575, 3.803421518355493, 4.119502029176264, 5.17521247781825, 5.902485659655832, 5.514018691588785, 4.918918918918919, 4.5, 1.6666666666666667, 0, 0]
availableAverage = [5.688069809846314, 5.814493805871942, 6.558681583854338, 7.117866816101616, 6.789674952198853, 7.383177570093458, 7.756756756756757, 7.7, 7.666666666666667, 0, 0]

#Initialize images
source_img = Image.open("field.jpg")
field = ImageDraw.Draw(source_img, "RGBA")

#Create Folders
now = datetime.now()
min = now.minute
hr = now.hour

#Adjust for easy readability
if len(str(min)) == 1:
    min = "0" + str(min)

if int(hr) > 12:
    hr -= 12

#Generate File Name
folderName = str(now.month) + "-" + str(now.day) + "-" + str(now.year)

#Create current days file if it doesnt exist
if not os.path.isdir(PATH + "/" + folderName):
    os.mkdir(PATH + "/" + folderName, 0o755)

#
#    Set up functions needed to draw and predict
#

#Generate range with certain intervals
def frange(x, y, jump):
    while x < y:
        yield x
        x += jump

#used to create/draw each datapoint
class dataPoint:
    def __init__(self, angle, hypotenuse, pred):
        angle = math.radians(angle)
        width = 1175
        distRange = 0

        adjacent = abs(math.cos(angle) * (hypotenuse / width))
        opposite = (math.sqrt(abs((hypotenuse * hypotenuse / width / width) - (adjacent * adjacent))))

        if angle < 0 and not opposite > 0:
            opposite = opposite * -1

        widthRatio = source_img.size[0]
        middle = ( source_img.size[1] / 2 )

        if angle < 0:
            opposite = opposite * -1

        adjacent = adjacent * widthRatio
        opposite = opposite * source_img.size[1] + middle

        self.angle = angle
        self.hypo = hypotenuse
        self.adjacent = adjacent
        self.opposite = opposite
        self.middle = middle
        self.pred = round(pred)

    #Draws each predction with defined opacity
    def draw_rectangle(self):
        halfWidth = 0.5
        layer = 1
        rect_start = (self.adjacent - halfWidth + layer, self.opposite - halfWidth + layer)
        rect_end = (self.adjacent + halfWidth - layer, self.opposite + halfWidth - layer)

        field.rectangle((rect_start, rect_end), outline = (200, 24, 24, self.pred))

#    ---------------------------------------------    #
#                                                     #
#    Main body function where everything is called    #
#                                                     #
#    ---------------------------------------------    #

#Create angles
jump = 0.1
negNintyToPos = frange(-90, 90, jump)

FIRST = True
for angle in negNintyToPos:
    if FIRST == True:
        startTime = datetime.now()
        width = 1175

        #Add points
    for dist in range(0, width):
        trueDist = dist / (width / FIELDWIDTH)

        distRange = 0
        available = 0

        for search in range(0, len(distanceRanges)):
            if round(trueDist) >= distanceRanges[search][0] and round(trueDist) <= distanceRanges[search][1]:
                distRange = hgtAverage[search]
                available = availableAverage[search]

        predictions = [abs((abs(angle) - 90) * weights[0]), trueDist * weights[1], abs(weights[2] * distRange), (available * weights[3])]

        pred = 0.0
        for x in predictions:
            pred += float(x)

        pred *= 20

        dp = dataPoint(float(angle), float(dist), pred)
        dp.draw_rectangle()

    #Estimate Time
    if FIRST == True:
        oneTime = datetime.now() - startTime
        time = (oneTime * 180) / jump
        timeStr = str(time)

        if timeStr[:5] == "0:00:":
            print("Estimated time till finish: " + timeStr + "s")
        elif timeStr[:2] == "0:" and timeStr[2:4] != "00":
            print("Estimated time till finish: " + str(time / 60) + "min")
        else:
            print("Estimated time till finish: " + str(time / 360) + "hrs")

        FIRST = False

#Save images
now = datetime.now()

#Alter Date Time from Military Time to Regular Time
hr = now.hour
if now.hour > 12:
    hr -= 12

#Generate filename from time
fileName = str(hr) + ":" + str(now.minute) + ":" + str(now.second)

#Save and show the image
source_img.show()
source_img.save(PATH + "/" + folderName + "/" + fileName + ".png", "PNG")
