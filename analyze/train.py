import numpy as np
import csv

f = open('MLSShotsData2011-2017.csv', encoding = "ISO-8859-1")
csv_f = csv.reader(f)
data = list(csv_f)

#Set start weights
weights = np.array([-0.12, 0.04, 0.03, 0.05])
alpha = 0.1

#Used to create
def vectMult(data):
    global weights
    floats = list()
    if len(data) != len(weights):
        raise Exception("\n\nLength of weights and inputs don't match")

    for x in range(0, len(data)):
        floats.append(data[x] * weights[x])

    return floats

def train(data):
    global weights
    #Goal, Miss, Blocked
    true = 0
    if str(data[20]) == "Goal":
        true = 1

    if data[18] == "NA":
        data[18] = 0

    total = 0
    #Take needed data from row
    condensedData = np.array([float(data[13]), abs(float(data[14])), float(data[18]), float(data[15])])
    error = 1

    #Do until the weights multiplied by data creates an acurate prediction
    while abs(error) > 0.92:
        #Mult data by weights to create outputs
        output = vectMult(condensedData)

        #Add all the outputs
        for x in output:
            total += x

        #Find how much the outputs missed by
        error = total - true

        #Alter weights
        for x in range(0, weights.size):
            weights[x] = weights[x] - (alpha * error)

#  ----------------------------------------------  #
#                                                  #
#  Main body function to call the other functions  #
#                                                  #
#  ----------------------------------------------  #

def main(data):
    for x in data[0:50000]:
        if not (x == data[0]):
            train(x)

            print("Row: " + str(data.index(x)))

#Call the main func and then print the final weights
main(data)

weightArray = []
for x in weights:
    weightArray.append(x)

print(weights)
print(weightArray)
