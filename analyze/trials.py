import csv

f = open('MLSShotsData2011-2017.csv', encoding = "ISO-8859-1")
csv_f = csv.reader(f)
data = list(csv_f)

weights = [-0.00219684, -0.00017413, -0.00045895, 0.00141542]
predictions = []

def vectMult(data):
    global weights
    floats = []
    if len(data) != len(weights):
        raise Exception("\n\nLength of weights and inputs don't match")
    try:
        for x in range(0, len(data)):
            floats.append(data[x] * weights[x])
    except TypeError:
        floats.append(data[x])

    return floats

for x in data:
    if x != data[0]:
        if x[18] == "NA":
            x[18] = 0

        condensedData = [float(x[13]), abs(float(x[14])), float(x[18]), float(x[15])]
        total = 0

        true = 0
        if str(data[20]) == "Goal":
            true = 1

        output = vectMult(condensedData)

        for y in output:
            total += y

        predictions.append(total)

max = 0.0
for x in predictions:
    if x > max:
        max = x

min = max
for x in predictions:
    if x < min:
        min = x

print(str(min) + " - " + str(max))
