import pandas as pd
import numpy as np
from sklearn import preprocessing
import matplotlib.pyplot as plt
from sklearn.linear_model import LogisticRegression
from sklearn.cross_validation import train_test_split
import seaborn as sns

plt.rc("font", size=14)

sns.set(style="white")
sns.set(style="whitegrid", color_codes=True)
