import csv

f = open('MLSShotsData2011-2017.csv', encoding = "ISO-8859-1")
csv_f = csv.reader(f)
data = list(csv_f)

weights = [-0.0022107819935719905, 0.00033603712557813656, 9.69683117601972e-05, 0.012574798306036278] #Use these

offcount = 0
count = 0

def vectMult(data):
    global weights
    floats = []
    if len(data) != len(weights):
        raise Exception("\n\nLength of weights and inputs don't match")
    try:
        for x in range(0, len(data)):
            floats.append(data[x] * weights[x])
    except TypeError:
        floats.append(data[x])

    return floats

for x in data[50000:]:
    if x != data[0]:
        if x[18] == "NA":
            x[18] = 0

        condensedData = [float(x[13]), abs(float(x[14])), float(x[18]), float(x[15])]
        true = 0
        total = 0

        if str(data[20]) == "Goal":
            true = 1

        output = vectMult(condensedData)

        for y in output:
            total += y

        error = total - true

        if abs(error) >= 0.05:
            offcount += 1
        count += 1

print(str(offcount) + " out of " + str(count))
print("About " + str(round(offcount / count * 100, 4)) + "% are wrong")
