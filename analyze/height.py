import csv

f = open('MLSShotsData2011-2017.csv', encoding = "ISO-8859-1")
csv_f = csv.reader(f)
data = list(csv_f)

numbers = ["zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten", "eleven"]
distances = [[0, 10], [11, 20], [21, 30], [31, 40], [41, 50], [51, 60], [61, 70], [71, 80], [81, 90], [91, 100], [101, 110]]
mapping = {0:"zero", 1:"one", 2:"two", 3:"three", 4:"four", 5:"five", 6:"six", 7:"seven", 8:"eight", 9:"nine", 10:"ten", 11:"eleven"}

for x in range(0, len(numbers)):
    exec(numbers[x] + " = 0")
    exec(numbers[x] + "Hgt = 0")

for x in data:
    if x != data[0]:
        found = False
        dist = abs(round(float(x[13])))
        if x[18] != "NA":
            rndheight = round(float(x[18]))
        else:
            rndheight = 0

        count = 0
        while found == False:
            if dist >= distances[count][0] and dist <= distances[count][1]:
                break
            count += 1

        exec(mapping[count] + " += 1")
        exec(mapping[count] + "Hgt += " + str(rndheight))

for x in distances:
    try:
        exec("print('" + numbers[distances.index(x)] + " : " + str(x) + " - ' + str(" + numbers[distances.index(x)] + "Hgt" + " / " + numbers[distances.index(x)] + ")" + "+ 'ft')")
    except ZeroDivisionError:
        exec("print('" + numbers[distances.index(x)] + " : " + str(x) + " - 0ft')")
