import csv

f = open('MLSShotsData2011-2017.csv')
csv_f = csv.reader(f)
data = list(csv_f)

weights = [-0.04703637176634485, -0.035935261766344574, 0.05295362823365531]
offcount = 0
count = 0

def vectMult(data):
    global weights
    floats = list()
    if len(data) != len(weights):
        raise Exception("\n\nLength of weights and inputs don't match")
    try:
        for x in range(0, len(data)):
            floats.append(data[x] * weights[x])
    except TypeError:
        floats.append(data[x])

    return floats

for x in data:
    if x != data[0]:
        condensedData = [float(x[13]), abs(float(x[14])), float(x[18])]
        true = 0
        total = 0

        if str(data[20]) == "Goal":
            true = 1

        output = vectMult(condensedData)

        for y in output:
            total += y

        error = total - true

        if error > 0.02:
            offcount += 1
        count += 1

print(str(offcount) + " out of " + str(count))
print(str(round(offcount / count * 100, 4)) + "% are wrong")
